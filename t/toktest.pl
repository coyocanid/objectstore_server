use strict;
use warnings;

use Data::Dumper;
use Test::More;

use lib './lib';
use Data::ObjectStore::Tokenizer;

my $tok = Data::ObjectStore::Tokenizer->new;


p( ";", ";" );
p( " ;", ";" );
p( "A B   Q D 'EEEEE';'zippo' 'zap", qw(A B Q D EEEEE ; zippo) );
p( "\\\'eeez';    'foop\\", "zap'eeez", ';' );
p( "'ooz' groo", "foop'ooz" ); #5
p( "vvvvy" );
p( "_thing;", 'groovvvvy_thing', ';' );
p( "'chow\\\'hound' 'bett\\''ty ", "chow'hound", "bett\'", 'ty' );
p( " oy\\'yo ", "oy'yo" );
p( " hello\\ there ", "hello there" );
p( q~ "look what\\ lovliens";you;have\'Here;~, "look what lovliens", ";", 'you', ';', "have'Here", ';' );
p( q~ "look what\\ lovliens";you;have'Here';~, "look what lovliens", ";", 'you', ';', "have'Here'", ';' );
done_testing;

sub p {
    my ( $str, @expected ) = @_;
    my $toks = $tok->tokenize( \$str );
#    print STDERR "$str -> [".join(" ",map { "<$_>" } @$toks)."]\n";
    is_deeply( $toks, \@expected, qq~"$str" => ~.join(" ",@expected) );
}
