#!/usr/bin/perl
use strict;
use warnings;

use IO::Socket::UNIX;

my $dir = "$ENV{HOME}/open_source/objectstore_server";
my $SOCK_PATH = "$dir/objectserver.sock";

print STDERR "Connecting to $SOCK_PATH\n";
my $client = IO::Socket::UNIX->new(
    Type => SOCK_STREAM(),
    Peer => $SOCK_PATH,
    ) or die "unable to connect to server : $!";
#binmode( $client, ':encoding(utf8)');
print STDERR "Connected\n";

eval {
    while( 1 ) {
        print ">";
        my $inp = <STDIN>;
        my $newl = $inp =~ /\n$/;
        print STDERR " STDIN <<$newl>> ".length($inp)." bytes\n$inp\n\n\n";
        while ($inp !~ /\n$/) {
            $inp .= <STDIN>;
            print STDERR " RESTDIN ".length($inp)." bytes\n";            
        }
        chop $inp;
        send_command( $inp );
        my $resp = get_response();
    }
};
if ($@) {
    die "ERROR : $@\n";
}


sub send_command {
    my $command = shift;
    my $len = 4 + length( $command );
    print STDERR "Sending $len bytes\n$command\n"; #includes the length itself
    $client->print( pack "L", $len );
    $client->print( $command );
}

sub get_response {
    my $data = '';
    $client->recv( $data, 1024 );
    my( $size, $resp ) = unpack "La*", $data;
    my $needs = $size - length( $resp );
    while ($needs > 0) {
        $client->recv( $data, 1024 );
        $resp .= $data;
        $needs = $size - length( $resp );
    }
    print STDERR "$$ got response from server '$resp'\n";
    return $data;
}


my $data = '';
$client->recv( $data, 1024 );
print STDERR "connected, got back $data\n";
$client->send( "END\n" );

$data = '';
$client->recv( $data, 1024 );
print STDERR "now, got back $data\n";
