#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;
use IO::Socket;
use IO::Socket::UNIX;
use IPC::SharedMem;
use IPC::SysV qw(IPC_PRIVATE S_IRWXU);
use Linux::Futex;
use Net::Server::Daemonize qw(daemonize is_root_user safe_fork);

use lib './lib';
use  Data::ObjectStore::ServerNode;

run();
exit(0);

# GLOBAL VARS ------------------------------------
my $server;    # server listener socket
my $SOCK_PATH; # path for UNIX FIFO socket
my $LOG;       # log filehandle
my $mutex;     # address of shared memory location
my $shm;       # shared memory
# GLOBAL VARS ------------------------------------

sub open_log {
    my $filename = shift;
    my $logdir = ( $filename =~ m~(.*)/[^/]+$~ );
    (-d $logdir) || mkdir $logdir;
    open ( $LOG, '>>', $filename ) or die "Unable to open logfile '$filename' : $@ $!";
}

sub logger {
    my $txt = shift;
    print STDERR "$txt\n";
    print $LOG "$txt\n";
}

sub run {

    print STDERR "daemon run called\n";
    # daemonize ---------------------------------------------
    if ( is_root_user ) {
        my $log_loc = "/var/log/objectserver/objectserver.log";
        open_log ( $log_loc );
        print "Logging to '$log_loc'\n";
        
        print STDERR "daemonizing\n";

        $SOCK_PATH = "/usr/lib/systemd/user/objectserver.sock";

        daemonize(
            'nobody',                         # User
            'nobody',                         # Group
            '/var/run/objectsserver.pid',
            );

    }
    else {
        my $dir = "$ENV{HOME}/open_source/objectstore_server";
        (-d $dir) || mkdir $dir;
        my $log_loc = "$dir/objectserver.log";
        open_log ( $log_loc );
        print "Logging to '$log_loc'\n";
        
        print STDERR "daemonizing\n";

        $SOCK_PATH = "$dir/objectserver.sock";

0 && # not daemonize for now        
        daemonize(
            $ENV{USER},                       # User 
            $ENV{USER},                       # Group
            "$dir/objectserver.pid",
            );
    }
    # daemonize ---------------------------------------------

    $SIG{INT} = sub {
        logger( "GOT SIG INT" );
        shut_down();
    };
    
    logger( 'starting up' );

    # shared mem Futex --------------------------------------
    $shm = IPC::SharedMem->new(IPC_PRIVATE, 8, S_IRWXU);
    $shm->write( '0', 0, 8 );
    # shared mem Futex --------------------------------------

    logger( 'initiated shared mem' );
    logger( "opening socket to '$SOCK_PATH'" );
    
    # unix socket listener ----------------------------------
    $server = IO::Socket::UNIX->new(
        Type => SOCK_STREAM(),
        Local => $SOCK_PATH,
        Listen => 1,
        );
    unless( $server ) {
        logger ( "Unable to start server : $?, $!" );
        die "Unable to start server : $?, $!";
    } 
#    binmode( $server, ':encoding(utf8)');
    # unix socket listener ----------------------------------

    logger( 'initiated socket' );
    
    # -------- create a mutex for Linux::Futex using Shared Memory ----
    my $shm = IPC::SharedMem->new(IPC_PRIVATE, 8, S_IRWXU);
    $shm->attach or die "$@ $!";
    $mutex = $shm->addr;
    Linux::Futex::init($mutex);
    
    # use Linux::Futex::lock($mutex) to lock
    # use Linux::Futex::unlock($mutex) to unlock
    # -------- create a mutex for Linux::Futex using Shared Memory ----

    # connection listen loop --------------------------------
    while (1) {
        eval {
            my $conn = $server->accept;
            logger "GOT connection\n";
            if (my $pid = safe_fork) {
            } else {
                # child
                $SIG{INT} = sub {
                    logger( "GOT SIG INT IN CHILD" );
                    shut_down();
                };
                
                handle_connection( $conn );
            }
        };
        if ($@) {
            logger "ERROR in listener loop : $@ $!";
        }
    }
    logger "DONE with loop" ;
    # connection listen loop --------------------------------    
} #run

sub shut_down {
    $server->close;
    logger "Server shutting down\n";
    unlink $SOCK_PATH;
    exit;
} #shutdown

sub handle_connection {
    my $conn = shift;
    #    binmode( $conn, ':encoding(utf8)');
    my $server_node = Data::ObjectStore::ServerNode->new;
    eval {
        
        while( 1 ) {
            my $data ='';
            $conn->recv( $data, 1024 );
            $server_node->accept_input( \$data );
            while ( my $resp = $server_node->process_next_statement ) {
                $conn->send( $resp );
#                $conn->send( "\n" );
            }
        }
    };
    if ($@) {
        logger( "$$ got error $@ $!" );
    }
    exit;
} #handle_connection

sub injest_data {
    
}

__END__

#some tests

for(1..10) {
    sleep 5;
    if (safe_fork) {
        my $data = $shm->read(0,8);
        open my $out, '>>', '/tmp/drun';
        $out->autoflush(1);
        print $out "$$ ITER ($data) $_\n";
        $shm->write( ++$data, 0, 8 );
        close $out;
        exit;
    }
}
close $out;

my $data = $shm->read(0,8);
open $out, '>>', '/tmp/drun';
$out->autoflush(1);
print $out " $$ FINALLY $data\n";
close $out;

