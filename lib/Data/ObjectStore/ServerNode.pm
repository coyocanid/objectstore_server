package Data::ObjectStore::ServerNode;

use strict;
use warnings;

use Data::ObjectStore::Tokenizer;
use Data::ObjectStore;

sub new {
    my ( $pkg, @args ) = @_;
    return bless {
        store => Data::ObjectStore->open_store( @args ),
        toker => Data::ObjectStore::Tokenizer->new,
        working_statement => [],
        ready_statements => [],
    }, shift;
}

sub accept_input {
    my ( $self, $data_ref ) = @_;
    my $toks = $$self->{toker}->tokenize( $data_ref );
    my $working = $self->{working_statement};
    for my $tok (@$toks) {
        if ($tok eq ';') {
            push @{$self->{ready_statements}}, $working;
            #clear working statement
            $working = $self->{working_statement} = [];
        }
        else {
            push @$working, $tok;
        }
    }
} #accept_input

sub process_next_statement {
    my $self = shift;
    if (my $statement = shift @{$self->{ready_statements}}) {
        return $self->process_statement( $statement );
    }
    return undef;
} #process_next_statement

sub _cantoks {
    my( $statement, $must, $may ) = @_;
    my %allowed = map { $_ => undef } @$must, @$may;
    while( my $tok = uc(shift @$statement) ) {
        if( $allowed{$tok} ) {
            my $next = shift @$statement;
            if ($next) {
                $allowed{$tok} = $next;
            }
            else {
                return 0, "$tok designator given without a value";
            }
        }
        else {
            return 0, "invalid token $tok given";
        }
    }
    unless ( @$must == grep { exists $allowed->{$_} } @$must ) {
        return 0, 'Missing required tokens';
    }
    return 1, \%allowed;
}

sub process_next_statement {
    my ( $self, $statement ) = @_;
    
    my $start = uc(shift @$statement); 
    if ($start eq 'LOCK') {
        if (@$statment==0) {
            return 0, 'LOCK requires at least one argument';
        }
        # check if its been called
        
        # check for valid lock names (not too big, no funny stuff)
        
    } #LOCK     
    elsif ($start eq 'UNLOCK') {
        # unlocks all locks
    } #UNLOCK
    
    elsif ($start eq 'REMOVE') {
        my $field = shift @$statement;
        my $verb = uc( shift @$statement );
        if ( $verb ne 'FROM' ) {
            return 0, 'REMOVE missing FROM clause';
        }
        my $from = shift @$statement;
        my $target = $from > 0 ? $store->fetch($from) : $store->fetch_by_path($from);
        my $r = ref( $target );
        if ( $r eq 'ARRAY' ) {
            return 0, 'REMOVE cannot be called on an array. Use LISTSPLICE';
        }
        elsif ($r eq 'HASH') {
            delete $target->{$field};
            return 1, 'REMOVE';
        }
        elsif ($r) {
            $target->remove_field($field);
            return 1, 'REMOVE';
        }
        return 0, "target '$from' not found";
    } #REMOVE

    elsif ($start eq 'ATTACH') {
        my ( $succ, $tokh ) = _cantoks( $statement, [qw( AS ON )], [qw( VAL REF CREATE CLASS )] );
        if (! $succ ) {
            return 0, $tokh;
        }
        my $field      = $tokh->{AS};
        my $target_ref = $tokh->{ON};
        my $target  = $target_ref > 0 ? $store->fetch($target_ref) : $store->fetch_by_path($target_ref);
        my $r = ref( $target );
        if ($r eq 'HASH') {
            if ($r->{$field}) {
                return 0, "FIELD '$field' already exists in hash '$target_ref'";
            }
        }
        elsif ($r eq 'ARRAY') {
            return 0, 'ATTACH cannot be used on array. Use LISTSPLICE instead.';
        }
        elsif ($r) {
            if ($r->has_field($field)) {
                return 0, "FIELD '$field' already exists in object '$target_ref'";
            }
        }
        else {
            return 0, 'No target to ATTACH to';
        }

        my $to_attach = $tokh->{VAL};
        if (my $t = $tokh->{REF}) {
            if( $to_attach ) {
                return 0, 'ATTACH cannot specify more than one of VAL REF or CREATE'
            }
            $to_attach = $t > 0 ? $store->fetch($t) : $store->fetch_by_path($t);
            if (!$to_attach) {
                return 0, 'ATTACH REF not found';
            }
        }
        if (my $t = $tokh->{CREATE}) {
            if( $to_attach ) {
                return 0, 'ATTACH cannot specify more than one of VAL REF or CREATE'
            }
            if ($t eq 'LIST') {
                $to_attach = [];
            }
            elsif ($teq 'HASH') {
                $to_attach = {};
            }
            elsif ($teq 'OBJECT') {
                $to_attach = $store->create_container( $tokh->{CLASS} );
            }
            else {
                return 0, 'ATTACH CREATE must be OBJECT HASH or LIST';
            }
        }

        if ($r eq 'HASH') {
            $r->{$field} = $to_attach;
        }
        else {
            $r->set( $field, $to_attach );
        }

        return 1, 'ATTACH';
    } #ATTACH
    
    elsif ($start eq 'MOVE') {
        my $field = shift @$statements;
        my ( $succ, $tokh ) = _cantoks( $statement, [qw( FROM TO )], [ 'ON' ] );
        if (! $succ ) {
            return 0, $tokh;
        }
        my $from_ref = $tokh->{FROM};
        my $from = $to_attach = $from_ref > 0 ? $store->fetch($from_ref) : $store->fetch_by_path($from_ref);
        my $val;
        my $r = ref( $from );
        if ($r eq 'HASH') {
            unless (exists $from->{$field}) {
                return 0, "MOVE : FIELD '$field' does not exists in hash '$from_ref'";
            }
            $val = $from->{$field};
        }
        elsif ($r eq 'ARRAY') {
            return 0, 'MOVE : cannot be used on array. Use LISTSPLICE instead.';
        }
        elsif ($r) {
            unless ($from->has_field($field)) {
                return 0, "MOVE : FIELD '$field' does not exist in object '$from_ref'";
            }
            $val = $from->get($field);
        }
        else {
            return 0, "MOVE: FROM '$from_ref' not found";
        }
        my $on;
        my $on_ref= $tokh->{ON};
        if ($on_ref) {
            $on = $on_ref > 0 ? $store->fetch($on_ref) : $store->fetch_by_path($on_ref);
            if (!$on) {
                return 0, "MOVE : ON '$on_ref' not found";
            }
        } else {
            $on_ref = $from_ref;
            $on = $from;
        }

        my $r = ref( $on );
        if ($r eq 'HASH') {
            if (exists $on->{$field}) {
                return 0, "MOVE : ON '$field' not exists in hash '$on_ref'";
            }
            $on->{$field} = $val;
        }
        elsif ($r eq 'ARRAY') {
            return 0, 'MOVE : ON cannot be used on array. Use LISTSPLICE instead.';
        }
        elsif ($r) {
            if ($from->has_field($field)) {
                return 0, "MOVE : ON '$field' exists in object '$on_ref'";
            }
            $on->set( $field, $val );
        }
        else {
            return 0, "MOVE: FROM '$from_ref' not found";
        }
        return 1, 'MOVE';

    } #MOVE
    
    elsif ($start eq 'LISTSPLICE') {
        my ( $succ, $tokh ) = _cantoks( $statement, [qw( ON POSITION REPLACE )] );
        if (! $succ ) {
            return 0, $tokh;
        }

        my $on_ref = $tokh->{ON};
        my $on = $on_ref > 0 ? $store->fetch($on_ref) : $store->fetch_by_path($on_ref);
        if ( ref( $on ) ne 'ARRAY' ) {
            return 0, 'LISTSPLICE: must be called ON array';
        }
        
        my $with = uc(shift @$statement);
        my @insert;
        if ($with eq 'WITH') {
            while ( my $type = uc(shift @$statement) ) {
                my $next = shift @$statement;
                unless ($next) {
                    return 0, "LISTSPLICE: WITH $type missing value";
                }
                if ($type eq 'VAL') {
                    push @insert, shift @$statement;
                }
                elsif ($type eq 'REF') {
                    my $ref = $next > 0 ? $store->fetch($next) : $store->fetch_by_path($next);
                    push @insert, $ref;
                }
                elsif ($type eq 'CREATE') {
                    my $new;
                    if ($next eq 'LIST') {
                        $new = [];
                    }
                    elsif ($next eq 'HASH') {
                        $new = {};
                    }
                    elsif ($next eq 'OBJECT') {
                        my $class;
                        if (uc($statement->[0]) eq 'CLASS') {
                            shift @$statement;
                            $class = shift @$statement;
                        }
                        $new = $store->create_container( $class );
                    }
                    else {
                        return 0, "LISTSPLICE: WITH has INVALID CREATE TYPE '$next'";
                    }
                    push @insert, $new;
                }
                else {
                    return 0, "LISTSPLICE: WITH invalid '$type'";
                }
            }
        }
        splice @$on, $tokh->{POSITION}, $tokh->{REPLACE}, @insert;
        return 1, 'LISTSPLICE';
    } #LISTSPLICE
    
    elsif ($start eq 'LISTPUSH') {
        my $on_des = uc(shift @$statement);
        if ( $on_des ne 'ON' ) {
            return 0, 'LISTPUSH: must have ON';
        }
        my $on_ref = shift @$statement;
        my $on = $on_ref > 0 ? $store->fetch($on_ref) : $store->fetch_by_path($on_ref);
        unless ($on) {
            return 0, "LISTPUSH : ON '$on_ref' not found";
        }
        my @insert;
        while ( my $type = uc(shift @$statement) ) {
            my $next = shift @$statement;
            unless ($next) {
                return 0, "LISTPUSH: WITH $type missing value";
            }
            if ($type eq 'VAL') {
                push @insert, shift @$statement;
            }
            elsif ($type eq 'REF') {
                my $ref = $next > 0 ? $store->fetch($next) : $store->fetch_by_path($next);
                push @insert, $ref;
            }
            elsif ($type eq 'CREATE') {
                my $new;
                if ($next eq 'LIST') {
                    $new = [];
                }
                elsif ($next eq 'HASH') {
                    $new = {};
                }
                elsif ($next eq 'OBJECT') {
                    my $class;
                    if (uc($statement->[0]) eq 'CLASS') {
                        shift @$statement;
                        $class = shift @$statement;
                    }
                    $new = $store->create_container( $class );
                }
                else {
                    return 0, "LISTPUSH: WITH has INVALID CREATE TYPE '$next'";
                }
                push @insert, $new;
            }
            else {
                return 0, "LISTPUSH: WITH invalid '$type'";
            }
        }
        push @$on, @insert;
        return 1, 'LISTPUSH';
    } #LISTPUSH


} #process_next_statement

"served the node";
