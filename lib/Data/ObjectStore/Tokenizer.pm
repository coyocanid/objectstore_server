package Data::ObjectStore::Tokenizer;

use strict;
use warnings;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(tokenize);

use Data::Dumper;

sub new {
    my $pkg = shift;
    # buff and stuff
    return bless ['',], $pkg;
}

sub tokenize {
    my( $self, $txt_ref ) = @_;

    my $txt = $self->[0] ? $self->[0] . $$txt_ref : $$txt_ref;
    my $endPos = length($txt) - 1;

    my $currPos = 0;
    my $tokenStartPosition = 0;
    my $inSingleQuote = 0;
    my $inDoubleQuote = 0;
    my $inToken = 0;

    my $tokens = [];

    while( 1 ) {

        if ($currPos > $endPos) {
            # end condition. Send back tokens collected
            # and save from tokenStartPosition
            if ($inToken) {
                $self->[0] = substr( $txt, $tokenStartPosition );
            }
            elsif ( $inSingleQuote || $inDoubleQuote) {
                $self->[0] = substr( $txt, $tokenStartPosition - 1 );
            }
            else {
                $self->[0] = '';
            }
            return $tokens;
        }

        if ($inSingleQuote) {
            my $nextEscape = index ($txt,"\\",$currPos+1);
            my $nextSingleQuote = index ($txt,"'",$currPos+1);
            if ($nextSingleQuote == -1 ) {
                $self->[0] = substr( $txt, $tokenStartPosition );
                return $tokens;
            }
            while ($nextEscape != -1 && $nextEscape < $nextSingleQuote) {
                $nextSingleQuote = index( $txt, "'", $nextEscape + 2 );
                $nextEscape = index( $txt, "\\", $nextEscape + 2 );
                if ($nextSingleQuote == -1) {
                    $self->[0] = substr( $txt, $tokenStartPosition );
                    return $tokens;
                }
            }
            # token is from tokenStartPosition until nextSingleQuote
            push @$tokens, _unescape(substr( $txt, 1 + $tokenStartPosition, ($nextSingleQuote-$tokenStartPosition)-1 ));
            $currPos = $nextSingleQuote + 1;
            $inSingleQuote = 0;
        }
        elsif ($inDoubleQuote) {
            my $nextEscape = index ($txt,"\\",$currPos+1);
            my $nextDoubleQuote = index ($txt,'"',$currPos+1);
            if ($nextDoubleQuote == -1 ) {
                $self->[0] = substr( $txt, $tokenStartPosition );
                return $tokens;
            }
            while ($nextEscape != -1 && $nextEscape < $nextDoubleQuote) {
                $nextDoubleQuote = index( $txt, '"', $nextEscape + 2 );
                $nextEscape = index( $txt, "\\", $nextEscape + 2 );
                if ($nextDoubleQuote == -1) {
                    $self->[0] = substr( $txt, $tokenStartPosition );
                    return $tokens;
                }
            }
            # token is from tokenStartPosition until nextDoubleQuote
            push @$tokens, _unescape(substr( $txt, 1 + $tokenStartPosition, ($nextDoubleQuote-$tokenStartPosition)-1 ));
            $currPos = $nextDoubleQuote + 1;
            $inDoubleQuote = 0;
        }
        elsif ($inToken) {
            # token ends on non escaped space or ; whichever comes first
            my $nextSemi   = index( $txt, ';', $tokenStartPosition );
            my $nextSpace  = index( $txt, ' ', $tokenStartPosition );
            my $nextEndToken = ($nextSemi != -1 && ($nextSemi < $nextSpace||$nextSpace==-1)) ? $nextSemi : $nextSpace;
            if ($nextEndToken == -1 ) {
                $self->[0] = substr( $txt, $tokenStartPosition );                
                return $tokens;
            }
            my $nextEscape = index ($txt,"\\",$currPos);
            if ($nextEscape != -1 && $nextEscape < $nextEndToken) {
                my $curr = $nextEscape + 2;
                $nextEscape = index ($txt,"\\", $curr);
                $nextSemi   = index( $txt, ';', $curr );
                $nextSpace  = index( $txt, ' ', $curr );
                $nextEndToken = ($nextSemi == -1 && $nextSemi < $nextSpace) ? $nextSemi : $nextSpace;
            }

            if ($nextSemi != -1 && ($nextSemi < $nextSpace||$nextSpace==-1)) {
                my $tok = _unescape(substr( $txt, $tokenStartPosition, $nextSemi - $tokenStartPosition ));
                if (length($tok)>0) {
                    push @$tokens, $tok;
                }
                push @$tokens, ';';
                $currPos = $nextSemi + 1;
                $inToken = 0;
            }
            elsif ($nextSpace != -1 ) {
                push @$tokens, _unescape(substr( $txt, $tokenStartPosition, $nextSpace - $tokenStartPosition ));
                $currPos = $nextSpace + 1;
                $inToken = 0;
            }
            else {
                $self->[0] = substr( $txt, $tokenStartPosition );
                return $tokens;
            }
        }
        else {
            # skip beginning spaces
            my $ord = ord( substr( $txt, $currPos ) );
            while ( ($ord > 8 && $ord < 14) || $ord == 32) {
                $currPos++;
                $ord = ord( substr( $txt, $currPos ) );
                if ($currPos > $endPos) {
                    #end condition. spaces only after the last token
                    $self->[0] = '';
                    return $tokens;
                }
            }
            if ( $ord == 39 ) {
                $inSingleQuote = 1;
                $tokenStartPosition = $currPos;
                $currPos++;
            }
            elsif ( $ord == 34 ) {
                $inDoubleQuote = 1;
                $tokenStartPosition = $currPos;
                $currPos++;
            }
            else {
                $inToken = 1;
                $tokenStartPosition = $currPos;
            }
        }
    }
} #tokenize


sub _unescape {
    my $txt = shift;
    my $start = 0;
    my $backpos = index( $txt, '\\', $start );
    if ($backpos != -1) {
        return substr( $txt, 0, $backpos ) . substr( $txt, $backpos + 1, 1 ) . _unescape( substr( $txt, $backpos + 2 ) );
    }
    return $txt;
}

"Tokenize";
